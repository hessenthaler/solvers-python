#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import scipy as sp
import scipy.sparse.linalg
import sys

def rnorm(A, x, b):
    # compute residual
    r               = np.dot(A, x) - b
    return np.linalg.norm(r, 2)

def print_matrix(A):
    n       = A.shape[0]
    for row in range(0, n, 1):
        print(np.array_repr(A[row,0:n:1]).replace('\n', ''))
    return