#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import scipy as sp
import scipy.sparse.linalg
import sys

def elimination(Ain, b):
    # work on copies
    A       = Ain.copy()
    n       = A.shape[0]
    x       = np.zeros(n)
    x       = b.copy()
    # make upper triangular form
    for col in range(0, n-1, 1):
        for row in range(col+1, n, 1):
            fact            = A[row,col] / A[col,col]
            A[row,0:n:1]   -= fact * A[col,0:n:1]
            x[row]         -= fact * x[col]
    # make diagonal form
    for col in range(n-1, 0, -1):
        for row in range(col-1, -1, -1):
            fact            = A[row,col] / A[col,col]
            A[row,0:n:1]   -= fact * A[col,0:n:1]
            x[row]         -= fact * x[col]
    # make unit diagonal form
    for row in range(0, n, 1):
        fact        = A[row,row]
        A[row,row] /= fact
        x[row]     /= fact
    return x.copy()