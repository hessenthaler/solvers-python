#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import scipy as sp
import scipy.sparse.linalg
import sys

def iteration(Ain, x0, b, maxiter, atol, weighted, isSPD):
    A           = Ain.copy()
    x           = x0.copy()
    # get diagonal and remainder
    D           = np.diag(np.diag(A))
    invD        = np.linalg.inv(D)
    R           = A.copy() - D.copy()
    invDA       = np.dot(invD, A)
    eigs        = np.sort(np.linalg.eigvals(invDA))
    # set default weight
    w           = 1.0
    if weighted:
        w           = 2.0 / 3.0
    if isSPD:
        weighted    = True
        # compute weight that minimizes spectral radius
        w           = 2.0 / (np.min(eigs) + np.max(eigs))
        # check if weight guarantuees convergence
        check       = 2.0 / np.linalg.norm(invDA, 2)
        while (w >= check):
            w       = 2.0 / 3.0 * w
    # solve fixed-point iteration
    iter        = 0
    converged   = False
    r           = np.dot(A, x) - b
    rnorm       = np.linalg.norm(r, 2)
    while(not(converged) and (iter < maxiter)):
        iter       += 1
        if weighted:
            x       = w * np.dot(invD, b - np.dot(R, x)) + (1.0 - w) * x
        else:
            x       = np.dot(invD, b - np.dot(R, x))
        r           = np.dot(A, x) - b
        rnorm       = np.linalg.norm(r, 2)
        converged   = (rnorm < atol)
    return x.copy(), converged, iter