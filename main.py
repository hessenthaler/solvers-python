#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import scipy as sp
import scipy.sparse.linalg
import sys
from src import gauss, jacobi, cg, utils

def main():
    # print header
    print("")
    print("    This code demonstrates the use of direct and iterative solvers for the system A*x=b.")
    print("")
    print("            Solve:    A * x = b     with A is random symmetric positive-definite matrix")
    print("                                         b is random right-hand-side")
    print("")
    # check number of commandline arguments
    if (len(sys.argv) != 4):
        print(">>>ERROR: Missing or invalid commandline arguments:")
        print(">>>ERROR:")
        print(">>>ERROR:     python3 main.py param1 param2 param3 param4 param5 param6 param7 param8")
        print(">>>ERROR:")
        print(">>>ERROR:            param1 - size of A")
        print(">>>ERROR:            param2 - absolute tolerance for iterative solvers")
        print(">>>ERROR:            param3 - maximum iterations for iterative solvers")
        return 1
    # set parameters
    n               = int(sys.argv[1])
    atol            = float(sys.argv[2])
    ni              = int(sys.argv[3])
    # initialize x0, b, and A as SPD
    A               = np.random.rand(n, n)
    A               = 0.5 * (A + A.T) + n * np.eye(n, dtype=float)
    x0              = np.zeros((n), dtype=float)
    b               = np.random.rand(n)
    ############ custom implementations
    print("")
    print(">>> Own implementation:")
    print("")
    # Gauss elimination
    x               = gauss.elimination(A, b)
    rnorm           = utils.rnorm(A, x, b)
    print("    Gauss elimination. Error: %.16g" % rnorm)
    # Jacobi iteration
    x, converged, i = jacobi.iteration(A, x0, b, ni, atol, True, True)
    rnorm           = utils.rnorm(A, x, b)
    print("    Jacobi iteration.  Error: %.16g" % rnorm)
    print("                       Iter:  %i" % i)
    if not(converged):
        print(">>>ERROR: Jacobi iteration did not converge.")
        return 1
    # Conjugate Gradient iteration
    x, converged, i = cg.iteration(A, x0, b, ni, atol)
    rnorm           = utils.rnorm(A, x, b)
    print("    CG iteration.      Error: %.16g" % rnorm)
    print("                       Iter:  %i" % i)
    if not(converged):
        print(">>>ERROR: CG iteration did not converge.")
        return 1
    ############ SciPy implementations
    print("")
    print(">>> SciPy:")
    print("")
    # direct solve
    x               = np.linalg.solve(A, b)
    rnorm           = utils.rnorm(A, x, b)
    print("    Direct solver.     Error: %.16g" % rnorm)
    # BIConjugate Gradient iteration
    x, info         = sp.sparse.linalg.bicg(A, b, x0=x0, tol=atol, maxiter=ni)
    if (info != 0):
        print(">>>ERROR: BICG solver did not converge. Error code: %i" % info)
        return 1
    rnorm           = utils.rnorm(A, x, b)
    print("    BICG solver.       Error: %.16g" % rnorm)
    # Conjugate Gradient iteration (SciPy)
    x, info         = sp.sparse.linalg.cg(A, b, x0=x0, tol=atol, maxiter=ni)
    if (info != 0):
        print(">>>ERROR: CG solver did not converge. Error code: %i" % info)
        return 1
    rnorm           = utils.rnorm(A, x, b)
    print("    CG solver.         Error: %.16g" % rnorm)
    # GMRES iteration
    x, info         = sp.sparse.linalg.gmres(A, b, x0=x0, tol=atol, maxiter=ni)
    if (info != 0):
        print("")
        print(">>>ERROR: GMRES solver did not converge. Error code: %i" % info)
        print("")
        return 1
    rnorm           = utils.rnorm(A, x, b)
    print("    GMRES solver.      Error: %.16g" % rnorm)
    # print footer
    print("")
    return 0

if __name__ == "__main__":
    main()