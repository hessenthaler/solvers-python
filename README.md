# solvers-python

This code demonstrates the use of direct and iterative solvers in Python.

## Operating systems

This code was developed for use with Linux operating systems.

## Installation requirements

The code depends on a number of third-party libraries:

* Python 3
* NumPy
* SciPy

## License

This code is distributed under the terms of both the MIT license and the Apache License (Version 2.0). Users may choose either license, at their option.

All new contributions must be made under both the MIT and Apache-2.0 licenses.

See [LICENSE-MIT](https://bitbucket.org/hessenthaler/solvers-python/src/master/LICENSE-MIT), [LICENSE-APACHE](https://bitbucket.org/hessenthaler/solvers-python/src/master/LICENSE-APACHE) and [COPYRIGHT](https://bitbucket.org/hessenthaler/solvers-python/src/master/COPYRIGHT) for details.
